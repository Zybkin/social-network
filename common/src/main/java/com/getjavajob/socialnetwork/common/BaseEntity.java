package com.getjavajob.socialnetwork.common;

import java.io.Serializable;

public interface BaseEntity extends Serializable {
    int getId();

    void setId(int id);
}
