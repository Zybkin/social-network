package com.getjavajob.socialnetwork.common;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "account_tbl")
public class Account implements BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "birth_day")
    private LocalDate birthDay;
    @Column(name = "home_address")
    private String homeAddress;
    @Column(name = "work_address")
    private String workAddress;
    @Column(unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    private String skype;
    private String telegram;
    @Column(name = "add_info")
    private String addInfo;
    @OneToMany(mappedBy = "account", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Phone> phones;
    private String avatar;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date")
    @Type(type = "org.hibernate.type.LocalDateType")
    private LocalDate registrationDate;

    @OneToMany(mappedBy = "fromAccount", cascade = CascadeType.ALL)
    private List<FriendRequest> requestsFromMe;
    @OneToMany(mappedBy = "toAccount", cascade = CascadeType.ALL)
    private List<FriendRequest> requestsToMe;

    public Account() {
        phones = new ArrayList<>();
        requestsFromMe = new LinkedList<>();
        requestsToMe = new LinkedList<>();
    }

    public Account(int id) {
        this.id = id;
        phones = new ArrayList<>();
        requestsFromMe = new LinkedList<>();
        requestsToMe = new LinkedList<>();
    }

    public Account(int id, String lastName, String firstName, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        phones = new ArrayList<>();
        requestsFromMe = new LinkedList<>();
        requestsToMe = new LinkedList<>();
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public List<FriendRequest> getRequestsFromMe() {
        return requestsFromMe;
    }

    public void setRequestsFromMe(List<FriendRequest> requestsFromMe) {
        this.requestsFromMe = requestsFromMe;
    }

    public List<FriendRequest> getRequestsToMe() {
        return requestsToMe;
    }

    public void setRequestsToMe(List<FriendRequest> requestsToMe) {
        this.requestsToMe = requestsToMe;
    }

    public List<Account> getFriends() {
        List<Account> friends = new ArrayList<>();
        List<FriendRequest> requests = requestsToMe;
        requests.addAll(requestsFromMe);
        for (FriendRequest request : requests) {
            if (request.isAccepted()) {
                Account a = request.getFromAccount().getId() == id ? request.getToAccount() : request.getFromAccount();
                friends.add(a);
            }
        }
        return friends;
    }

    public List<Account> getRequestedToMe() {
        List<Account> followers = new ArrayList<>();
        for (FriendRequest request : requestsToMe) {
            if (request.getToAccount().equals(this) && !request.isAccepted()) {
                followers.add(request.getFromAccount());
            }
        }
        return followers;
    }

    public List<Account> getRequestedFromMe() {
        List<Account> followed = new ArrayList<>();
        for (FriendRequest request : requestsFromMe) {
            if (request.getFromAccount().equals(this) && !request.isAccepted()) {
                followed.add(request.getToAccount());
            }
        }
        return followed;
    }

    public void addRequestsFromMe(Account to) {
        FriendRequest newRequest = new FriendRequest(this, to);
        this.requestsFromMe.add(newRequest);
        to.requestsToMe.add(newRequest);
    }

    public void addRequestsToMe(Account from) {
        FriendRequest newRequest = new FriendRequest(from, this);
        this.requestsToMe.add(newRequest);
        from.requestsFromMe.add(newRequest);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return email.equals(account.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }
}
