package com.getjavajob.socialnetwork.common;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries(
        @NamedQuery(name = "getAllAccountRequests",
        query = "SELECT f FROM FriendRequest f WHERE f.fromAccount.id = :from OR f.toAccount.id = :to")
)
@Entity
@Table(name = "friends_tbl")
public class FriendRequest implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account fromAccount;
    @Id
    @ManyToOne
    @JoinColumn(name = "friend_id")
    private Account toAccount;
    @Column(name = "response", nullable = false)
    private boolean accepted;

    public FriendRequest() {
    }

    public FriendRequest(Account fromAccount, Account toAccount) {
        this(fromAccount, toAccount, false);
    }

    public FriendRequest(Account fromAccount, Account toAccount, boolean accepted) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.accepted = accepted;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean response) {
        this.accepted = response;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FriendRequest that = (FriendRequest) o;

        if (accepted != that.accepted) return false;
        if (fromAccount != null ? !fromAccount.equals(that.fromAccount) : that.fromAccount != null) return false;
        return toAccount != null ? toAccount.equals(that.toAccount) : that.toAccount == null;
    }

    @Override
    public int hashCode() {
        int result = fromAccount != null ? fromAccount.hashCode() : 0;
        result = 31 * result + (toAccount != null ? toAccount.hashCode() : 0);
        result = 31 * result + (accepted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FriendRequest{" +
                "fromAccount=" + fromAccount +
                ", toAccount=" + toAccount +
                ", accepted=" + accepted +
                '}';
    }
}
