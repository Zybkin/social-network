package com.getjavajob.socialnetwork.common;

public class AccountDTO {
    private int id;
    private String firstName;
    private String lastName;
    private String addInfo;

    public AccountDTO(Account account) {
        this.id = account.getId();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.addInfo = account.getAddInfo();
        if (addInfo == null || addInfo.isEmpty()) {
            this.addInfo = "Great Mind";
        }
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getAddInfo() {
        return addInfo;
    }
}
