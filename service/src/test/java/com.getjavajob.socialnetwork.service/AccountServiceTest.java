package com.getjavajob.socialnetwork.service;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.common.Phone;
import com.getjavajob.socialnetwork.dao.AccountDAO;
import com.getjavajob.socialnetwork.dao.FriendDAO;
import com.getjavajob.socialnetwork.dao.PhoneDAO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @Mock
    private FriendDAO friendDAO;
    @Mock
    private AccountDAO accountDAO;
    @Mock
    private PhoneDAO phoneDAO;
    @InjectMocks
    private AccountService accountService;
    private Account account;
    private Account friend;
    private Phone phone1;
    private Phone phone2;

    @Before
    public void setUp() throws Exception {
        phone1 = new Phone(String.valueOf(new Random().nextInt() * 100000));
        phone2 = new Phone(String.valueOf(new Random().nextInt() * 100000));
        account = new Account(1, "Ivanov", "Ivan", "123", "123");
        account.setPhones(Arrays.asList(phone1, phone2));
        friend = new Account(2, "Stepanov", "Sasha", "124", "124");
    }

    @Test
    public void createAccount() throws Exception {
        accountService.createAccount(account);
        verify(accountDAO).create(account);
    }

    @Test
    public void update() throws Exception {
        accountService.update(account);
        verify(accountDAO).update(account);
        verify(phoneDAO, times(2)).create(anyObject());
    }

    @Test
    public void delete() throws Exception {
        accountService.delete(account);
        verify(accountDAO).delete(account);
    }

    @Test
    public void getById() throws Exception {
        int accountId = account.getId();
        when(accountDAO.get(accountId)).thenReturn(account);

        assertEquals(account, accountService.getById(accountId));
        verify(accountDAO).get(accountId);
    }

    @Test
    public void getAll() throws Exception {
        when(accountDAO.getAll()).thenReturn(Collections.singletonList(account));

        assertEquals(Collections.singletonList(account), accountService.getAll());
        verify(accountDAO).getAll();
    }
}