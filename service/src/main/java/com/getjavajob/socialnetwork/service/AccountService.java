package com.getjavajob.socialnetwork.service;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.common.FriendRequest;
import com.getjavajob.socialnetwork.common.Phone;
import com.getjavajob.socialnetwork.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Transactional
@Service
public class AccountService {

    @Value("${pageSize}")
    private int pageSize;
    @Value("${autocompleteSize}")
    private int autocompleteSize;

    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private FriendDAO friendDAO;
    @Autowired
    private PhoneDAO phoneDAO;

    @Autowired
    private AccountRepository accountRepository;

    public Account createAccount(Account account) {
        return accountDAO.create(account);
    }

    public void update(Account account) {
        accountDAO.update(account);
    }

    public Account getByEmail(String email) {
//        if (email == null) {
//            return null;
//        }
//        return accountDAO.getByEmail(email);
        return accountRepository.getByEmail(email);
    }

    public List<Phone> getAccountPhones(int accountId) {
        return phoneDAO.getByAccountId(accountId);
    }

    public void delete(Account entity) {
        accountDAO.delete(entity);
    }

    public Account getById(int id) {
        return accountDAO.get(id);
    }

    public List<Account> getAll() {
        List<Account> accounts = accountDAO.getAll();
        accounts.forEach(account -> {
            account.setPhones(phoneDAO.getByAccountId(account.getId()));
        });
        return accounts;

    }

    public boolean isLogged(String email, String pass) {
        if (email == null || pass == null) {
            return false;
        }
        Account account = accountDAO.getByEmail(email);
        return account != null && account.getPassword().equals(pass);
    }

    public List<Account> getSearchAccount(String partName, int page) {
        Page<Account> accountPage = accountRepository.getBySearchPattern(partName, new PageRequest((page - 1), pageSize));
        return accountPage.getContent();
    }

    public int getSearchPages(String partName) {
        Page<Account> page = accountRepository.getBySearchPattern(partName, new PageRequest(0, pageSize));
        return page.getTotalPages();
    }

    public List<Account> getSearchAutocomplete(String partName) {
        return accountRepository.getBySearchPatternAutocomplete(partName, new PageRequest(0, autocompleteSize));
    }

    public void sendFriendRequest(Account from, Account to) {
        Account accountFrom = getById(from.getId());
        Account accountTo = getById(to.getId());
        accountFrom.addRequestsFromMe(accountTo);
    }

    public void acceptFriendRequest(Account from, Account to) {
        Account accountFrom = getById(from.getId());
        Account accountTo = getById(to.getId());
        friendDAO.acceptRequest(accountFrom, accountTo);
    }

    public void rejectFriend(Account from, Account to) {
        FriendRequest requestFromMe = friendDAO.getFriendRequest(from, to);
        FriendRequest requestToMe = friendDAO.getFriendRequest(to, from);
        if (requestFromMe == null && requestToMe != null) {
            friendDAO.rejectRequest(to, from);
        } else if (requestFromMe != null) {
            friendDAO.removeRequest(from, to);
            sendFriendRequest(to, from);
        }
    }

    public void removeFriend(Account from, Account to) {
        Account accountFrom = getById(from.getId());
        Account accountTo = getById(to.getId());
        if (friendDAO.getFriendRequest(from, to) != null) {
            friendDAO.removeRequest(accountFrom, accountTo);
        } else {
            friendDAO.removeRequest(accountTo, accountFrom);
        }
    }

    public List<Account> getFriendsList(Account account) {
        List<Account> friends = new LinkedList<>();
        friends.addAll(getById(account.getId()).getFriends());
        return friends;
    }

    public List<Account> getFollowersList(Account account) {
        List<Account> followers = new LinkedList<>();
        followers.addAll(getById(account.getId()).getRequestedToMe());
        return followers;
    }

    public List<Account> getFollowedList(Account account) {
        List<Account> followed = new LinkedList<>();
        followed.addAll(getById(account.getId()).getRequestedFromMe());
        return followed;
    }

    @Transactional
    public Relation getRelation(int fromId, int toId) {
        if (fromId == toId) {
            return Relation.ME;
        }
        Account from = accountDAO.get(fromId);
        Account to = accountDAO.get(toId);
        FriendRequest requestReceived = friendDAO.getFriendRequest(to, from);
        FriendRequest request = requestReceived != null ? requestReceived : friendDAO.getFriendRequest(from, to);
        if (request == null) {
            return Relation.STRANGER;
        } else if (request.isAccepted()) {
            return Relation.FRIEND;
        } else if (request.getFromAccount().getId() == fromId) {
            return Relation.FOLLOWED;
        } else if (request.getToAccount().getId() == fromId) {
            return Relation.FOLLOWER;
        } else {
            return Relation.ME;
        }
    }
}
