package com.getjavajob.socialnetwork.service;

import com.getjavajob.socialnetwork.common.BaseEntity;
import com.getjavajob.socialnetwork.dao.AbstractDAO;

import java.util.List;

public class AbstractService<E extends BaseEntity> {
    private AbstractDAO<E> dao;

    public AbstractService() {
    }

    public AbstractService(AbstractDAO<E> dao) {
        this.dao = dao;
    }

    public void delete(E entity) {
        dao.delete(entity);
    }

    public E getById(int id) {
        return dao.get(id);
    }

    public List<E> getAll() {
        return dao.getAll();
    }
}
