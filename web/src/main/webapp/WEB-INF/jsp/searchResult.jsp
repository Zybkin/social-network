<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Search result</title>

    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/account.css" type="text/css"/>
</head>
<body>
<%@ include file="menuPanel.jsp" %>
<div class="container">
    <div class="col-sm-10 col-sm-offset-1">
        <table class="table table-hover" style="background: white">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody id="account-results">
            <c:forEach var="account" items="${accounts}">
                <tr onclick="window.document.location = '${pageContext.request.contextPath}/account/${account.id}/home'">
                    <td class="col-md-2">${account.firstName}</td>
                    <td class="col-md-2">${account.lastName}</td>
                    <td class="col-md-8">${account.addInfo}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <ul class="pagination">
            <c:forEach var="index" begin="1" end="${accountPages}">
                <li class="page">
                    <button class="btn btn-default" name="${index}">${index}</button>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
<div id="pattern" hidden>${pattern}</div>
<%@include file="footer.jsp" %>
</body>
</html>