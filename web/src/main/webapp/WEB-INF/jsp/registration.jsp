<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css" type="text/css"/>
</head>
<body>

<%@ include file="visitorHeader.jsp" %>
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <form role="form" action="${pageContext.request.contextPath}/do-register" method="post">
                    <h2>Sign Up</h2>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="firstName" id="firstName" class="form-control input-lg"
                                       placeholder="First Name" tabindex="1" required>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="lastName" id="lastName" class="form-control input-lg"
                                       placeholder="Last Name" tabindex="2" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control input-lg"
                               placeholder="Email address" tabindex="3" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control input-lg"
                               placeholder="Password" tabindex="4" required>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <button type="submit" class="btn btn-primary btn-block btn-lg"
                                    tabindex="5">Register</button>
                        </div>
                        <div class="col-xs-6 col-md-6"><a href="${pageContext.request.contextPath}/login"
                                                          class="btn btn-success btn-block btn-lg">Sign In</a></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
