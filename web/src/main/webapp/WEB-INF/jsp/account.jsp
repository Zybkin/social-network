<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Page ${account.firstName}</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/account.css" type="text/css"/>
</head>
<body>
<%@ include file="menuPanel.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-md-3 page-sidebar">
            <!-- SIDEBAR USERPIC -->
            <div class="col-sm-12 form-group">
                <c:choose>
                    <c:when test="${account.avatar != null}">
                        <img src="data:image/jpeg;base64,${account.avatar}" format="jpg" invalidate="true"
                             class="img-thumbnail"/>
                    </c:when>
                    <c:otherwise>
                        <img src="${pageContext.request.contextPath}/img/account-default.png" class="img-thumbnail">
                    </c:otherwise>
                </c:choose>
            </div>
            <!-- END SIDEBAR USERPIC -->

            <!-- SIDEBAR BUTTONS -->
            <div class="col-sm-12 btn-group-vertical">
                <c:if test='${relation == "ME"}'>
                    <a type="button" class="btn btn-primary " href="${pageContext.request.contextPath}/following">Following</a>
                    <a type="button" class="btn btn-primary " href="${pageContext.request.contextPath}/followers">Followers</a>
                </c:if>
                <c:if test='${relation == "STRANGER"}'>
                    <a type="button" class="btn btn-primary "
                       href="${pageContext.request.contextPath}/sendFriendRequest?toId=${account.id}">Add to friends</a>
                </c:if>
                <c:if test='${relation == "FOLLOWER"}'>
                    <a type="button" class="btn btn-primary "
                       href="${pageContext.request.contextPath}/acceptFriendRequest?fromId=${account.id}">Accept
                        friendship</a>
                </c:if>
                <c:if test='${relation == "FOLLOWED"}'>
                    <a type="button" class="btn btn-primary  disabled">Request sent</a>
                    <a type="button" class="btn btn-primary"
                       href="${pageContext.request.contextPath}/deleteRequest?accId=${account.id}">Withdraw
                        request</a>
                </c:if>
                <c:if test='${relation == "FRIEND"}'>
                    <a type="button" class="btn btn-primary"
                       href="${pageContext.request.contextPath}/rejectFriend?accId=${account.id}">Remove from
                        friends</a>
                </c:if>
                <%--<c:if test='${relation != "ME"}'>--%>
                    <%--<a type="button" class="btn btn-primary "--%>
                       <%--href="${pageContext.request.contextPath}/messages?recipientId=${account.id}">Send message</a>--%>
                <%--</c:if>--%>

                <a type="button" class="btn btn-primary"
                   href="${pageContext.request.contextPath}/account/${account.id}/friends">Friends</a>
                <%--<a type="button" class="btn btn-primary"--%>
                   <%--href="${pageContext.request.contextPath}/account/${account.id}/groups">Groups</a>--%>

                <%--</div>--%>
                <!-- END SIDEBAR BUTTONS -->
            </div>
        </div>

        <div class="col-md-8">
            <div class="page-content">
                <div class="profile-user">
                    <h3 class="text-left profile-user-name">
                        <c:out value='${account.firstName}'/>
                        <c:if test='${account.middleName != null}'>
                            <c:out value='${account.middleName}'/>
                        </c:if>
                        <c:out value='${account.lastName}'/>
                    </h3>
                    <table class="table">
                        <tbody>
                        <c:choose>
                            <c:when test='${account.birthDay != null}'>
                                <tr>
                                    <td class="col-md-5">Birthday:</td>
                                    <td><c:out value='${account.birthDay}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.email != null}'>
                                <tr>
                                    <td class="col-md-5">Email:</td>
                                    <td><c:out value='${account.email}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${not empty account.homeAddress}'>
                                <tr>
                                    <td class="col-md-5">Home Address:</td>
                                    <td><c:out value='${account.homeAddress}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${not empty account.workAddress}'>
                                <tr>
                                    <td class="col-md-5">Work Address:</td>
                                    <td><c:out value='${account.workAddress}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${not empty account.telegram}'>
                                <tr>
                                    <td class="col-md-5">Telegram:</td>
                                    <td><c:out value='${account.telegram}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${not empty account.skype}'>
                                <tr>
                                    <td class="col-md-5">Skype:</td>
                                    <td><c:out value='${account.skype}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${not empty phones}'>
                                <tr>
                                    <td class="col-md-5">Phones:</td>
                                    <td>
                                        <c:forEach var='ph' items='${phones}'>
                                            <c:out value="${ph.number}"/><br>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${not empty account.addInfo}'>
                                <tr>
                                    <td class="col-md-5">About me:</td>
                                    <td><c:out value='${account.addInfo}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.registrationDate != null}'>
                                <tr>
                                    <td class="col-md-5">Registration date:</td>
                                    <td><c:out value='${account.registrationDate}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>