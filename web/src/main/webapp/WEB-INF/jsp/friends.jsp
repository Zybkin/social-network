<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Search result</title>

    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/account.css" type="text/css"/>
</head>
<body>
<%@ include file="menuPanel.jsp" %>
<div class="container">
    <div class="col-sm-10 col-sm-offset-1">
        <h3>${account.firstName} ${account.lastName}'s ${typeOfAccount}</h3>
        <c:choose>
            <c:when test="${empty friends}">
                <h4>You can use the search form to find ${typeOfAccount}</h4>
            </c:when>
            <c:otherwise>
                <table class="table table-hover" style="background: white">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="account" items="${friends}">
                        <tr onclick="window.document.location = '${pageContext.request.contextPath}/account/${account.id}/home'">
                            <td class="col-md-2">${account.firstName}</td>
                            <td class="col-md-2">${account.lastName}</td>
                            <td class="col-md-8">${account.addInfo}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:otherwise>
        </c:choose>
    </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>