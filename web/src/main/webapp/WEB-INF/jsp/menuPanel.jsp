<script>var ctx = "${pageContext.request.contextPath}"</script>
<nav class="navbar navbar-fixed-top navbar-inverse" id="menu-panel">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href='${pageContext.request.contextPath}/account/${sessionScope.account.id}/home'>Great Minds</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class=${MenuBean.home}>
                    <a href='${pageContext.request.contextPath}/account/${sessionScope.account.id}/home'>Home</a></li>
                <li class=${MenuBean.friends}>
                    <a href="${pageContext.request.contextPath}/account/${sessionScope.account.id}/friends">Friends</a></li>
                </li>
                <li class=${MenuBean.groups}>
                    <%--<a href="${pageContext.request.contextPath}/account/${sessionScope.get("id")}/groups">Groups</a>--%>
                    <a href='#'>Groups</a></li>
                </li>
                <li class=${MenuBean.messages}>
                    <%--<a href="${pageContext.request.contextPath}/messages">My Messages</a>--%>
                    <a href='#'>Messages</a></li>
                </li>
                <li class=${MenuBean.settings}>
                    <a href="${pageContext.request.contextPath}/settings">Settings</a>
                </li>
            </ul>

            <form class="navbar-form navbar-left" role="search" method="get" id="search-form"
                  action="${pageContext.request.contextPath}/search">
                <div class="input-group">
                    <input id="search-input" type="search" class="form-control" name="part-name"
                           placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            Search
                        </button>
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <span class="navbar-text">
                    <c:out value='${sessionScope.account.lastName}'/>
                    <c:out value='${sessionScope.account.firstName}'/>
                    </span>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout">
                        <span class="glyphicon glyphicon-log-out"></span> Logout
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>