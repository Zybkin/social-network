<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Great Minds: log in</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css" type="text/css"/>
</head>
<body>

<%@ include file="visitorHeader.jsp" %>

<div class="container">

    <form id="loginForm" class="form-signin" method="post" action="<c:url value='/do-login'/>">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" id="inputEmail" class="input-email form-control" name="username" placeholder="Email address"
               required
               autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
        <div class="btn-group">
            <button class="btn btn-lg btn-primary btn-login" type="submit" form="loginForm">Log in</button>
            <a class="btn btn-lg btn-success btn-register" href="${pageContext.request.contextPath}/registration">Register</a>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="rememberMe"> Remember me
            </label>
        </div>
        <c:if test="${not empty error}">
            <div class="alert alert-danger">
                <c:out value='${error}'/>
            </div>
        </c:if>
        <c:if test="${not empty logout}">
            <div class="alert alert-success">
                <c:out value='${logout}'/>
            </div>
        </c:if>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>

</div> <!-- /container -->
</body>
</html>
