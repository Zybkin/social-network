<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Edit page ${account.firstName}</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/settings.css" type="text/css"/>

</head>
<body>
<%@ include file="menuPanel.jsp" %>
<div class="container">
    <div id="dialog-confirm" title="Are you sure about that?" style="visibility: hidden"></div>

    <div class="col-md-offset-3 col-md-6" style="background: white; vertical-align: middle">
        <h1 class="text-center">Edit page of ${account.firstName} ${account.lastName}</h1>

        <form id="updateForm" class="form-horizontal" action="<c:url value="/set-settings"/>"
              method="post" enctype="multipart/form-data">
            <input class="form-account-id" type="hidden" name="id" value="${account.id}">
            <div class="form-group">
                <label for="input-image" class="col-md-4 control-label">Upload image</label>
                <div class="col-md-8">
                    <label id="chooseAvatarBtn" class="btn btn-default">
                        Change avatar
                        <input type="file" name="avatar" id="input-image" style="display: none" t>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Last Name</label>
                <div class="col-md-8">
                    <input class="form-control form-last-name" maxlength="30" type="text" name="lastName" id="lastName"
                           placeholder="Last Name" value="${account.lastName}" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">First Name</label>
                <div class="col-md-8">
                    <input class="form-control form-first-name" maxlength="30" type="text" name="firstName"
                           placeholder="First Name" value="${account.firstName}" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Middle Name</label>
                <div class="col-md-8">
                    <input class="form-control form-middle-name" maxlength="30" type="text" name="middleName"
                           placeholder="Middle Name" value="${account.middleName}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Birth day</label>
                <div class="col-md-8">
                    <input class="form-control form-birthday" type="date" name="birthDay" placeholder="Birth day"
                           value="${account.birthDay}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Email</label>
                <div class="col-md-8">
                    <input class="form-control form-email" type="text" name="email" placeholder="email"
                           value="${account.email}"
                           required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Telegram</label>
                <div class="col-md-8">
                    <input class="form-control form-telegram" type="text" name="telegram" placeholder="telegram"
                           value="${account.telegram}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Skype</label>
                <div class="col-md-8">
                    <input class="form-control form-skype" type="text" name="skype" placeholder="skype"
                           value="${account.skype}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Home address</label>
                <div class="col-md-8">
                    <input class="form-control form-home-address" type="text" name="homeAddress"
                           placeholder="home address"
                           value="${account.homeAddress}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Work address</label>
                <div class="col-md-8">
                    <input class="form-control form-work-address" type="text" name="workAddress"
                           placeholder="work address"
                           value="${account.workAddress}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Additional information</label>
                <div class="col-md-8">
                    <textarea class="form-control form-add-info noresize" name="addInfo"
                              rows="3">${account.addInfo}</textarea>
                </div>
            </div>

            <%--ADD PHONES (JS SUPPORTED)--%>
            <div class="form-group">
                <label class="col-md-4 control-label">Phones</label>
                <div class="col-md-8">
                    <div class="form-group-phones">
                        <div style="margin-bottom: 10px">
                            <a class="btn btn-success add-phone-button">Add phone</a>
                        </div>
                        <div class="form-group-phones-wrapper">
                            <c:forEach var="ph" items='${account.phones}'>
                                <div class="input-group input-phone">
                                    <input class="form-control form-phone-number" name="phones.number"
                                           value="${ph.number}"
                                           placeholder="Add phone" type="text"/>
                                    <input class="form-phone-id" name="phone.id" value="${ph.id}" type="hidden"/>
                                    <div class="input-group-btn">
                                        <button class="remove_phone_button btn btn-danger">
                                            del
                                        </button>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
            <%--XML data transfer (JS SUPPORTED)--%>
            <div class="form-group">
                <label class="col-md-4 control-label">Data transfer</label>
                <div class="col-md-4">
                    <label class="btn btn-success btn-block to-xml-button">
                        Save to XML
                    </label>
                    <a class="to-xml-download-file" target="_blank" download="account.xml" hidden="true"></a>
                </div>
                <div class="col-md-4">
                    <label class="btn btn-success btn-block from-xml-label">
                        Load from XML
                        <input class="from-xml-input" style="display: none" type="file" name="avatar">
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">New password</label>
                <div class="col-md-8">
                    <input class="form-control" name="newPassword" id="newPassword" placeholder="New password"
                           type="password"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">New password (again)</label>
                <div class="col-md-8">
                    <input class="form-control" name="newPasswordAgain" id="newPasswordAgain"
                           placeholder="New password (again)"
                           type="password"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Old password</label>
                <div class="col-md-8">
                    <input class="form-control" name="password" placeholder="Old password" required type="password"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="button" class="btn btn-primary pull-right" id="changeBtn">Change information
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="okCancelModel" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Save changes?</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to submit the following details?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="saveBtn btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="dialog-error hidden">
                <p>All nodes in XML file must have camel case name</p>
            </div>

        </form>
    </div>
</div> <!-- /container -->
<%@include file="footer.jsp" %>
<script src="${pageContext.request.contextPath}/js/settings.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/xmlParser.js" type="text/javascript"></script>
</body>
</html>