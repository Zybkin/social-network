$(document).ready(function () {
    var search = $("#search-input");
    search.autocomplete({
        source: function (request, response) {
            $.ajax({
                // url: window.location.origin + '/autocomplete',
                url: ctx + '/autocomplete',
                data: {
                    pattern: request.term
                },
                success: function (data) {
                    response($.map(data, function (entry, i) {
                        return {
                            valueId: entry.id,
                            label: entry.fullName
                        }
                    }));
                }
            });
        },
        position: {
            my: "left+0 top+6"
        },
        select: function (event, ui) {
            event.preventDefault();
            location.pathname = ctx + "/account/" + ui.item.valueId + "/home";
        },
        minLength: 1
    });
});

$(document).ready(
    $(".page").click(function () {
        var partName = document.getElementById("pattern").innerHTML;
        var pageNum = $(this).find("button").attr("name");
        $.ajax({
            url: ctx + '/getPage',
            data: {
                pattern: partName,
                page: pageNum
            },
            success: function (data) {
                var tableResults = $("#account-results");
                tableResults.empty();
                $.each(data, function (index, account) {
                    var accountUrl = ctx + '/account/' + account.id + '/home';
                    tableResults.append(
                    "<tr onclick=\"window.document.location = '" + accountUrl + "'\">"
                        + "<td class='col-md-2'>" + account.firstName + "</td>"
                        + "<td class='col-md-2'>" + account.lastName + "</td>"
                        + "<td class='col-md-8'>" + account.addInfo + "</td>"
                        + "</tr>"
                    );
                })
            }
        });
    })
);