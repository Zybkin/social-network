$(document).ready(function () {
    var inputXml = document.getElementsByClassName("from-xml-input")[0];
    var toXmlButton = document.getElementsByClassName("to-xml-button")[0];

    inputXml.addEventListener("change", uploadFromXml, false);
    toXmlButton.addEventListener("click", createAndDownloadXml, false);

    /**
     * If file exists than parse it and change input's values for account
     */
    function uploadFromXml(e) {
        var files = e.target.files;
        if (files[0]) {
            var reader = new FileReader();
            reader.onload = function () {
                try {
                    var parsed = new DOMParser().parseFromString(this.result, "text/xml");
                    $(".form-last-name").val(getValue(parsed.getElementsByTagName("lastName")[0].childNodes[0]));
                    $(".form-first-name").val(getValue(parsed.getElementsByTagName("firstName")[0].childNodes[0]));
                    $(".form-middle-name").val(getValue(parsed.getElementsByTagName("middleName")[0].childNodes[0]));
                    $(".form-birthday").val(getValue(parsed.getElementsByTagName("birthDay")[0].childNodes[0]));
                    $(".form-email").val(getValue(parsed.getElementsByTagName("email")[0].childNodes[0]));
                    $(".form-telegram").val(getValue(parsed.getElementsByTagName("telegram")[0].childNodes[0]));
                    $(".form-skype").val(getValue(parsed.getElementsByTagName("skype")[0].childNodes[0]));
                    $(".form-home-address").val(getValue(parsed.getElementsByTagName("homeAddress")[0].childNodes[0]));
                    $(".form-work-address").val(getValue(parsed.getElementsByTagName("workAddress")[0].childNodes[0]));
                    $(".form-add-info").val(getValue(parsed.getElementsByTagName("addInfo")[0].childNodes[0]));

                    var numbers = parsed.getElementsByTagName("phone");
                    var phWrapper = $(".form-group-phones-wrapper");
                    phWrapper.empty();
                    for (var i = 0, l = numbers.length; i < l; i++) {
                        addPhone(phWrapper, numbers[i].childNodes[0].nodeValue)
                    }
                } catch (e) {
                    $("<div>All nodes in XML file must have camel case name</div>").dialog({
                        title: "Incorrect data in XML",
                        resizable: false,
                        modal: true
                    });
                }
            };
            reader.readAsText(files[0]);
        }
    }

    /**
     * Get value of element if it not null else returns empty string
     */
    function getValue(elem) {
        if (elem) {
            return elem.nodeValue;
        }
        return "";
    }

    /**
     * Add div phone-wrapper on the form
     *
     * @param number -- number of phone from xml
     */
    function addPhone(phWrapper, number) {
        phWrapper.append('<div class="input-group input-phone">' +
            '   <input class="form-control form-phone-number" name="phones.number" placeholder="Add phone"' +
            '       value="' + number + '" type="text"/>' +
            '   <input class="form-phone-id" name="phones.id" value="0" type="hidden"/>' +
            '   <div class="input-group-btn">' +
            '       <button class="remove_phone_button btn btn-danger">del' +
            '</button></div></div>');
    }

    /**
     * Parse form, get values from inputs and generate xml file. Then simulate click on new file's url
     */
    function createAndDownloadXml() {
        data = [];
        data.push("\<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
        data.push("<account>\n");
        data.push("\t<lastName>" + $(".form-last-name").val() + "</lastName>\n");
        data.push("\t<firstName>" + $(".form-first-name").val() + "</firstName>\n");
        data.push("\t<middleName>" + $(".form-middle-name").val() + "</middleName>\n");
        data.push("\t<birthDay>" + $(".form-birthday").val() + "</birthDay>\n");
        data.push("\t<email>" + $(".form-email").val() + "</email>\n");
        data.push("\t<telegram>" + $(".form-telegram").val() + "</telegram>\n");
        data.push("\t<skype>" + $(".form-skype").val() + "</skype>\n");
        data.push("\t<homeAddress>" + $(".form-home-address").val() + "</homeAddress>\n");
        data.push("\t<workAddress>" + $(".form-work-address").val() + "</workAddress>\n");
        data.push("\t<addInfo>" + $(".form-add-info").val() + "</addInfo>\n");
        $(".form-phone-number").each(function () {
            data.push("\t<phone>" + $(this).val() + "</phone>\n");
        });
        data.push("</account>\n");

        var properties = {type: 'plain/xml'};
        var file;
        try {
            // Specify the filename using the File constructor, but ...
            file = new File(data, "file.xml", properties);
        } catch (e) {
            // ... fall back to the Blob constructor if that isn't supported.
            file = new Blob(data, properties);
        }
        var url = URL.createObjectURL(file);
        var toXmlDownloadFile = document.getElementsByClassName("to-xml-download-file")[0];
        toXmlDownloadFile.href = url;
        toXmlDownloadFile.click();
    }
});