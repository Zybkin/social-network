$(document).ready(function () {
    var form = $("#updateForm");
    var valid = true;
    var formPhones = $(".form-group-phones");       // Fields formPhones
    var phWrapper = $(".form-group-phones-wrapper");// Fields formPhones
    var addButton = $(".add-phone-button");         // Add button class
    var saveButton = $(".saveBtn");                 // Save button on dialog form
    var changeButton = $("#changeBtn");             // Change button on main form
    var maxPhoneFields = 10;                        // Maximum phone boxes allowed
    var countPhones = document.getElementsByName("phones").length;
    var inputImage = document.getElementById("input-image");
    var appendPhoneWrapper = '' +
        '<div class="input-group input-phone">' +
        '   <input class="form-control form-phone-number" name="phones.number" placeholder="Add phone" type="text"/>' +
        '   <input class="form-phone-id" name="phones.id" value="0" type="hidden"/>' +
        '   <div class="input-group-btn">' +
        '       <button class="remove_phone_button btn btn-danger">del' +
        '</button></div></div>';    // Phone wrapper for account
    /**
     * Check count phone boxes and add one box if it is allowed
     */
    $(addButton).click(function (e) {
        e.preventDefault();
        if (countPhones < maxPhoneFields) {
            addPhone();
        }
    });

    function addPhone() {
        $(phWrapper).append(appendPhoneWrapper);
        countPhones++;
    }

    /**
     * Catch user click on remove phone button. Remove phone box and decrement countPhones
     */
    $(formPhones).on("click", ".remove_phone_button", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        countPhones--;
    });

    /**
     * Prepare phone's inputs for Spring MVC and submit form
     */
    $(saveButton).on("click", function (e) {
        e.preventDefault();
        renamePhoneInputs();
        form.submit();
    });

    /**
     * Validate form and if all fields valid open dialog form
     */
    $(changeButton).on("click", function (e) {
        e.preventDefault();
        validate();
        if (valid) {
            $('#okCancelModel').modal('show');
        }
    });

    /**
     * Main form validation
     */
    function validate() {
        valid = true;
        var phones = document.getElementsByName("phones.number");
        phones.forEach(validatePhone);

        var reqInputElems = document.getElementById("updateForm").querySelectorAll("[required]");
        reqInputElems.forEach(validRequiredElements);
        validNewPassword();
    }

    function renamePhoneInputs() {
        valid = true;
        var phoneIds = document.getElementsByClassName("form-phone-id");
        var phonesNumbers = document.getElementsByClassName("form-phone-number");
        for (var i = phonesNumbers.length - 1; i >= 0; --i) {
            console.log(i);
            phonesNumbers[i].name = "phones[" + i + "].number";
        }
        for (var j = phoneIds.length - 1; j >= 0; --j) {
            console.log(j);
            phoneIds[j].name = "phones[" + j + "].id";
        }
    }

    function validNewPassword() {
        var newPass = document.getElementById("newPassword");
        var newPassAgain = document.getElementById("newPasswordAgain");
        if (newPass.value != newPassAgain.value) {
            newPass.parentNode.classList.add("has-error");
            newPassAgain.parentNode.classList.add("has-error");
            valid = false;
        } else {
            newPassAgain.parentNode.classList.remove("has-error");
            newPass.parentNode.classList.remove("has-error");
        }
    }

    function validRequiredElements(elem) {
        if (!elem.value) {
            elem.parentElement.classList.add("has-error");
            valid = false;
        } else if (elem.parentElement.classList.contains("has-error")) {
            elem.parentElement.classList.remove("has-error");
        }
    }

    function validatePhone(phone) {
        var reg = /\D/g;
        arg = phone.value;
        if (!arg || arg.match(reg) || arg.length == 0) {
            phone.parentNode.classList.add("has-error");
            valid = false;
        } else {
            if (phone.parentNode.classList.contains("has-error")) {
                phone.parentNode.classList.remove("has-error");
            }
        }
    }

    if (inputImage) {
        inputImage.addEventListener("change", imageBtnChangeColor, false);
    }
    function imageBtnChangeColor(e) {
        var imgInput = e.target;
        if (imgInput.files[0]) {
            imgInput.parentNode.className = "btn btn-success";
        } else {
            imgInput.parentNode.className = "btn btn-default";
        }
    }
});