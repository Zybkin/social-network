package com.getjavajob.socialnetwork.web;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class AuthInterceptor extends HandlerInterceptorAdapter {

    private final List<String> allowLinks = Arrays.asList(
            "/login", "/registration", "/do-login", "/do-register", "/bootstrap" , "/css", "/js");

    @Autowired
    AccountService accountService;

    @Override
    public boolean preHandle(HttpServletRequest req,
                             HttpServletResponse resp, Object handler) throws Exception {

        String requestURI = req.getRequestURI();
        for (String link : allowLinks) {
            if (requestURI.contains(link)) {
                return true;
            }
        }

        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        if (sessionAccount != null) {
            if (accountService.isLogged(sessionAccount.getEmail(), sessionAccount.getPassword())) {
                return true;
            } else {
                resp.sendRedirect(req.getContextPath() + "/login");
                return false;
            }
        } else {
            Cookie[] cookies = req.getCookies();
            if (cookies != null) {
                String email = null;
                String password = null;
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("username")) {
                        email = cookie.getValue();
                        continue;
                    }
                    if (cookie.getName().equals("password")) {
                        password = cookie.getValue();
                    }
                }

                if (email != null && password != null) {
                    if (accountService.isLogged(email, password)) {
                        Account account = accountService.getByEmail(email);
                        req.getSession().setAttribute("account", account);
                        return true;
                    }
                }
            }
        }
        resp.sendRedirect(req.getContextPath() + "/login");
        return false;
    }
}