package com.getjavajob.socialnetwork.web;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.common.AccountDTO;
import com.getjavajob.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class SearchController {
    @Autowired
    private AccountService service;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    protected ModelAndView search(@RequestParam("part-name") String partName) throws ServletException, IOException {
        ModelAndView view = new ModelAndView("searchResult");
        List<Account> accounts = service.getSearchAccount(partName, 1);
        List<AccountDTO> searchModels = accounts.stream().map(AccountDTO::new).collect(Collectors.toList());
        view.addObject("part-name", partName);
        view.addObject("accountPages", service.getSearchPages(partName));
        view.addObject("accounts", searchModels);
        return view;
    }

    @ResponseBody
    @RequestMapping(value = "/getPage", method = RequestMethod.GET)
    protected List<AccountDTO> getAccountPage(@RequestParam("pattern") String partName, @RequestParam("page") int page) {
        List<Account> list = service.getSearchAccount(partName, page);
        return list.stream().map(AccountDTO::new).collect(Collectors.toList());
    }

    @ResponseBody
    @RequestMapping(value = "/autocomplete", method = RequestMethod.GET)
    protected List<AccountDTO> findSearchModels(@RequestParam("pattern") String partName) {
        List<Account> list = service.getSearchAutocomplete(partName);
        return list.stream().map(AccountDTO::new).collect(Collectors.toList());
    }
}
