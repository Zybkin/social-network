package com.getjavajob.socialnetwork.web;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.getjavajob.socialnetwork.web.Util.Sha256.getHash;

@Controller
public class SecurityController {

    @Autowired
    AccountService accountService;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView view = new ModelAndView("login");
        if (error != null) {
            view.addObject("error", error);
        }
        return view;
    }

    @RequestMapping(value = "do-login", method = RequestMethod.POST)
    public String doLogin(@RequestParam(value = "username") String username,
                          @RequestParam(value = "password") String password,
                          @RequestParam(value = "rememberMe", required = false) boolean remember,
                          HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String hashPass = getHash(password);
        if (accountService.isLogged(username, hashPass)) {
            Account account = accountService.getByEmail(username);
            session.setAttribute("account", account);
            session.removeAttribute("error");
            if (remember) {
                Cookie login = new Cookie("username", account.getEmail());
                Cookie psw = new Cookie("password", account.getPassword());
                resp.addCookie(login);
                resp.addCookie(psw);
            }
            return "redirect:/account/" + account.getId() + "/home";
        } else {
            session.setAttribute("error", "Invalid username or password!");
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest req, HttpServletResponse resp, HttpSession session) {
        session.invalidate();
        resp.addCookie(removeCookie("username"));
        resp.addCookie(removeCookie("password"));
        ModelAndView view = new ModelAndView("login");
        view.addObject("logout", "You successfully log out");
        return view;
    }

    private Cookie removeCookie(String cookieName) {
        Cookie cookie = new Cookie(cookieName, "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        return cookie;
    }
}
