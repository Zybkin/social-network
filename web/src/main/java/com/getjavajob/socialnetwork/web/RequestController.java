package com.getjavajob.socialnetwork.web;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RequestController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/sendFriendRequest", method = RequestMethod.GET)
    public ModelAndView sendFriendRequest(@SessionAttribute("account") Account me, @RequestParam int toId) {
        Account from = accountService.getById(me.getId());
        Account to = accountService.getById(toId);
        accountService.sendFriendRequest(from, to);
        return new ModelAndView("redirect:/account/" + toId + "/home");
    }

    @RequestMapping(value = "/acceptFriendRequest", method = RequestMethod.GET)
    public String acceptFriendRequest(@SessionAttribute("account") Account me, @RequestParam int fromId) {
        Account from = accountService.getById(fromId);
        Account to = accountService.getById(me.getId());
        accountService.acceptFriendRequest(from, to);
        return "redirect:/account/" + fromId + "/home";
    }

    @RequestMapping(value = "/deleteRequest", method = RequestMethod.GET)
    public String deleteRequest(@SessionAttribute("account") Account me, @RequestParam int accId) {
        Account from = accountService.getById(me.getId());
        Account to = accountService.getById(accId);
        accountService.removeFriend(from, to);
        return "redirect:/account/" + accId + "/home";
    }

    @RequestMapping(value = "/rejectFriend", method = RequestMethod.GET)
    public String removeFriend(@SessionAttribute("account") Account me, @RequestParam int accId) {
        Account from = accountService.getById(me.getId());
        Account to = accountService.getById(accId);
        accountService.rejectFriend(from, to);
        return "redirect:/account/" + accId + "/home";
    }
}
