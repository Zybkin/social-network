package com.getjavajob.socialnetwork.web;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.common.AccountDTO;
import com.getjavajob.socialnetwork.common.Phone;
import com.getjavajob.socialnetwork.service.AccountService;
import com.getjavajob.socialnetwork.web.Util.Sha256;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.getjavajob.socialnetwork.web.Util.AppHelper.getBytesFromImgStream;
import static com.getjavajob.socialnetwork.web.Util.Sha256.getHash;

@Controller
public class AccountController {
    private final String ERROR_BUSY_EMAIL = "This email occupied";

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/account/{id}/home", method = RequestMethod.GET)
    public ModelAndView accountHomePage(@PathVariable("id") int id, HttpSession session) {
        Account account = accountService.getById(id);
        List<Phone> phones = accountService.getAccountPhones(id);
        Account sessionAccount = (Account) session.getAttribute("account");
        String relation = accountService.getRelation(sessionAccount.getId(), id).name();
        ModelAndView modelAndView = new ModelAndView("account");
        modelAndView.addObject("account", account);
        modelAndView.addObject("phones", phones);
        modelAndView.addObject("relation", relation);
        return modelAndView;
    }

    @RequestMapping(value = "/account/{id}/friends", method = RequestMethod.GET)
    public ModelAndView showFriends(@PathVariable("id") int id) {
        Account account = accountService.getById(id);
        List<Account> friendsList = accountService.getFriendsList(account);
        List<AccountDTO> friends = friendsList.stream().map(AccountDTO::new).collect(Collectors.toList());
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("account", account);
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("typeOfAccount", "friends");
        return modelAndView;
    }

    @RequestMapping(value = "/followers", method = RequestMethod.GET)
    public ModelAndView showFollowers(@SessionAttribute(value = "account", required = false) Account sessionAccount) {
        Account account = accountService.getById(sessionAccount.getId());
        List<Account> followersList = accountService.getFollowersList(account);
        List<AccountDTO> friends = followersList.stream().map(AccountDTO::new).collect(Collectors.toList());
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("account", account);
        modelAndView.addObject("friends", followersList);
        modelAndView.addObject("typeOfAccount", "followers");
        return modelAndView;
    }

    @RequestMapping(value = "/following", method = RequestMethod.GET)
    public ModelAndView showFollowing(@SessionAttribute(value = "account", required = false) Account sessionAccount) {
        Account account = accountService.getById(sessionAccount.getId());
        List<Account> followedList = accountService.getFollowedList(account);
        List<AccountDTO> friends = followedList.stream().map(AccountDTO::new).collect(Collectors.toList());
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("account", account);
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("typeOfAccount", "following");
        return modelAndView;
    }

    @RequestMapping("home")
    public String home(@SessionAttribute(value = "account", required = false) Account account,
                       HttpServletRequest req) {
        if (account != null) {
            return "redirect:/account/" + account.getId() + "/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping("/registration")
    public String goRegistration() {
        return "registration";
    }

    @RequestMapping(value = "/do-register", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute Account account, HttpSession session, HttpServletRequest req) {
        if (accountService.getByEmail(account.getEmail()) != null) {
            req.setAttribute("error", ERROR_BUSY_EMAIL);
            return "redirect:/login";
        } else {
            account.setPassword(Sha256.getHash(account.getPassword()));
            accountService.createAccount(account);
            session.setAttribute("account", account);
            session.setAttribute("id", account.getId());
            return "redirect:/account/" + account.getId() + "/home";
        }
    }

    @RequestMapping("settings")
    public String goSettings(HttpSession session) {
        return "settings";
    }

    @RequestMapping(value = "/set-settings", method = RequestMethod.POST)
    public String doSetting(@ModelAttribute("account") Account account,
                             BindingResult result,
                             @RequestParam(value = "newPassword", required = false) String newPassword,
                             @RequestParam(value = "avatar", required = false) MultipartFile avatar,
                             @SessionAttribute("account") Account oldAccount,
                             HttpSession session) throws IOException {
        String hashOldPass = getHash(account.getPassword());
        if (!oldAccount.getPassword().equals(hashOldPass)) {
            return "redirect:/settings";
        }
        account.getPhones().removeIf(phone -> phone.getNumber() == null);
        account.getPhones().forEach(phone -> phone.setAccount(account));
        if (newPassword != null && !newPassword.isEmpty()) {
            account.setPassword(getHash(newPassword));
        } else {
            account.setPassword(hashOldPass);
        }
        if (avatar != null && avatar.getSize() != 0) {
            account.setAvatar(getBytesFromImgStream(avatar.getInputStream()));
        } else {
            account.setAvatar(oldAccount.getAvatar());
        }

        accountService.update(account);
        session.setAttribute("account", account);
        return "redirect:/account/" + account.getId() + "/home";
    }

    @InitBinder("account")
    public void customModel(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, "birthDay", new PropertyEditorSupport() {
            @Override
            public String getAsText() {
                return getValue().toString();
            }

            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(LocalDate.parse(text));
            }
        });
        binder.registerCustomEditor(ArrayList.class, "phone", new CustomCollectionEditor(ArrayList.class));
    }


}