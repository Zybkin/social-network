package com.getjavajob.socialnetwork.web.Util;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static javax.xml.bind.DatatypeConverter.printBase64Binary;

public class AppHelper {

    public static String getBytesFromImgStream(InputStream stream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int nRead = stream.read(buffer);
        while (nRead != -1) {
            byteArrayOutputStream.write(buffer, 0, nRead);
            nRead = stream.read(buffer);
        }
        return printBase64Binary(byteArrayOutputStream.toByteArray());
    }

    public static <T> T getBean(ServletContext context, String beanName) {
        WebApplicationContext appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
        return (T) appContext.getBean(beanName);
    }
}
