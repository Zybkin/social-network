package com.getjavajob.socialnetwork.dao;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.common.Phone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:daoContext.xml", "classpath:daoContextOverrides.xml"})
public class PhoneDAOTest {

    private static final Resource CREATE_TABLES = new ClassPathResource("create_tables.sql");
    private static final Resource INSERT = new ClassPathResource("insert.sql");

    @Autowired
    private PhoneDAO phoneDAO;
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private DataSource dataSource;

    private ResourceDatabasePopulator databasePopulator;

    @Before
    public void setUp() throws Exception {
        databasePopulator = new ResourceDatabasePopulator(CREATE_TABLES, INSERT);
        databasePopulator.execute(dataSource);
    }

    @Test
    public void update() throws Exception {
        String s = "newMail";
        Phone newPhone = getPhone(1, s);
        newPhone.setId(1);

        phoneDAO.update(newPhone);
        assertEquals(s, phoneDAO.get(1).getNumber());
    }

    @Test
    public void create() throws Exception {
        Phone phone = getPhone(1, "google");
        int beforeCount = phoneDAO.getAll().size();

        phoneDAO.create(phone);
        assertEquals(beforeCount + 1, phoneDAO.getAll().size());
    }

    @Test
    public void delete() throws Exception {
        int countBeforeDelete = phoneDAO.getAll().size();
        System.out.println(countBeforeDelete);

        Phone phone = new Phone(1);
        phoneDAO.delete(phone);
        assertEquals(countBeforeDelete - 1, phoneDAO.getAll().size());
    }

    @Test
    public void get() throws Exception {
        String testFirstInsertedPhone = "123";
        Phone gettingMail = phoneDAO.get(1);

        assertNotNull(gettingMail);
        assertEquals(testFirstInsertedPhone, gettingMail.getNumber());
    }

    @Test
    public void getAll() throws Exception {
        int countInsertedPhones = 4;
        assertEquals(countInsertedPhones, phoneDAO.getAll().size());

        phoneDAO.create(getPhone(1, "mail1"));
        phoneDAO.create(getPhone(1, "mail2"));
        int countAccounts = 2;

        assertEquals(countAccounts + countInsertedPhones, phoneDAO.getAll().size());
    }

    private Account getAccount() {
        Account account = new Account();
        account.setLastName("Kutorkin");
        account.setFirstName("Vasya");
        account.setEmail("1234");
        account.setPassword("1234");
        return account;
    }

    private Phone getPhone(int accountId, String number) {
        Phone phone = new Phone();
        phone.setAccount(accountDAO.get(accountId));
        phone.setNumber(number);
        return phone;
    }

}