package com.getjavajob.socialnetwork.dao;

import com.getjavajob.socialnetwork.common.Account;
import com.getjavajob.socialnetwork.common.Phone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:daoContext.xml", "classpath:daoContextOverrides.xml"})
public class AccountDAOTest {

    private final static Resource CREATE_TABLES = new ClassPathResource("create_tables.sql");
    private final static Resource INSERT = new ClassPathResource("insert.sql");

    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private javax.sql.DataSource dataSource;

    private String testEmail = "1111";

    @Before
    public void setUp() throws Exception {
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(CREATE_TABLES, INSERT);
        databasePopulator.execute(dataSource);
    }

    @Test
    public void add() throws Exception {
        Account account = getNewAccount("1234");
        int beforeCount = accountDAO.getAll().size();

        accountDAO.create(account);
        assertEquals(beforeCount + 1, accountDAO.getAll().size());
    }

    @Test
    public void update() throws Exception {
        String email = "1212";
        Account account = getNewAccount(email);
        account.setId(1);

        accountDAO.update(account);
        assertEquals(email, accountDAO.get(1).getEmail());
    }

    @Test
    public void getAccountAndUpdate() throws Exception {
        Phone phone = getPhone(1, "123123");
        phone.setId(5);
        Account account = getNewAccount("email");
        account.setId(1);
        account.setPhones(Collections.singletonList(phone));

        assertEquals(1,accountDAO.get(1).getPhones().size());
        accountDAO.update(account);
        List<Phone> phones = accountDAO.get(1).getPhones();
//        assertEquals(phone, phones.get(0));
        assertEquals(1, phones.size());
//        assertEquals(, accountDAO.get(1).getEmail());
    }

    @Test
    public void delete() throws Exception {
        int countBeforeDelete = accountDAO.getAll().size();

        Account account = new Account(1);
        accountDAO.delete(account);
        assertEquals(countBeforeDelete - 1, accountDAO.getAll().size());
    }

    @Test
    public void get() throws Exception {
        Account account = accountDAO.get(1);
        int countPhones = account.getPhones().size();
        assertNotNull(account);
        assertEquals(1, countPhones);
    }

    @Test
    public void getByEmail() throws Exception {
        accountDAO.create(getNewAccount(testEmail));
        Account accountTest = accountDAO.getByEmail(testEmail);

        assertNotNull(accountTest);
        assertEquals(testEmail, accountTest.getEmail());
    }

    @Test
    public void getAll() throws Exception {
        int accountsInInsertScript = 4;
        assertEquals(accountsInInsertScript, accountDAO.getAll().size());

        Account account = getNewAccount("1234");
        Account account1 = getNewAccount("4321");
        accountDAO.create(account);
        accountDAO.create(account1);

        int countAccounts = 2;

        assertEquals(countAccounts + accountsInInsertScript, accountDAO.getAll().size());
    }

    private Account getNewAccount(String email) {
        Account account = new Account();
        account.setLastName("Kutorkin");
        account.setFirstName("Vasya");
        account.setEmail(email);
        account.setPassword("1234");
        return account;
    }

    private Phone getPhone(int accountId, String number) {
        Phone phone = new Phone();
        phone.setAccount(accountDAO.get(accountId));
        phone.setNumber(number);
        return phone;
    }
}