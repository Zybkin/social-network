package com.getjavajob.socialnetwork.dao;

import com.getjavajob.socialnetwork.common.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractDAO<E extends BaseEntity> implements CrudDAO<E> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractDAO.class);
    protected static final String START_METHOD = "Start method ";
    protected static final String DELETE = "Delete from DB entity ";

    @PersistenceContext
    protected EntityManager entityManager;

    private Class<E> entityClass;

    public AbstractDAO(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    protected CriteriaQuery<E> getEntityQuery(CriteriaBuilder builder) {
        return builder.createQuery(entityClass);
    }

    protected Root<E> getEntityRoot(CriteriaQuery<?> query) {
        return query.from(entityClass);
    }

    protected abstract Predicate getSearchWherePredicate(String pattern, CriteriaBuilder builder, Root<E> root);

    @Override
    public void update(E entity) {
        logger.info(START_METHOD + "update");
        if (entity == null) {
            return;
        }
        entityManager.merge(entity);
        logger.debug("Updated entity " + entity);
    }

    @Override
    public List<E> getAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = getEntityQuery(builder);
        Root<E> from = getEntityRoot(query);
        query.select(from);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public E get(int id) {
        if (id <= 0) {
            return null;
        }
        E entity = entityManager.find(entityClass, id);
        return entity;
    }

    @Override
    public E create(E entity) {
        logger.info(START_METHOD + "create");
        entityManager.merge(entity);
        logger.debug("Create entity " + entity);
        return entity;
    }

    public long getSearchEntitiesCount(String pattern) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<E> root = getEntityRoot(query);
        query.select(builder.count(root)).where(getSearchWherePredicate(pattern, builder, root));
        return entityManager.createQuery(query).getSingleResult();
    }

    public List<E> getSearchEntities(String pattern, int firstRow, int count) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = getEntityQuery(builder);
        Root<E> root = getEntityRoot(query);
        query.select(root).where(getSearchWherePredicate(pattern, builder, root));
        return entityManager.createQuery(query).setFirstResult(firstRow).setMaxResults(count).getResultList();
    }
}
