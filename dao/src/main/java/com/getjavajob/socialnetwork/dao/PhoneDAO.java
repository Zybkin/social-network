package com.getjavajob.socialnetwork.dao;

import com.getjavajob.socialnetwork.common.Phone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class PhoneDAO extends AbstractDAO<Phone> {
    private static final Logger logger = LoggerFactory.getLogger(PhoneDAO.class);

    public PhoneDAO() {
        super(Phone.class);
    }

    @Override
    public void delete(Phone entity) {
        logger.info(START_METHOD + "delete phone");
        Phone delPhone = entityManager.find(Phone.class, entity.getId());
        entityManager.remove(delPhone);
        logger.debug(DELETE + entity);
    }

    public List<Phone> getByAccountId(int accountId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Phone> query = builder.createQuery(Phone.class);
        Root<Phone> phoneRoot = query.from(Phone.class);
        query.select(phoneRoot).where(builder.equal(phoneRoot.get("account"), accountId));
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    protected Predicate getSearchWherePredicate(String pattern, CriteriaBuilder builder, Root<Phone> root) {
        return null;
    }
}
