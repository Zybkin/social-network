package com.getjavajob.socialnetwork.dao;

import java.util.List;

public interface CrudDAO<E> {

    E create(E entity);

    void update(E entity);

    void delete(E entity);

    E get(int id);

    List<E> getAll();
}
