package com.getjavajob.socialnetwork.dao;

public enum Relation {
    FRIEND, FOLLOWER, FOLLOWED, STRANGER, ME
}
