package com.getjavajob.socialnetwork.dao;

import com.getjavajob.socialnetwork.common.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository
public class AccountDAO extends AbstractDAO<Account> {
    private static final Logger logger = LoggerFactory.getLogger(AccountDAO.class);

    public AccountDAO() {
        super(Account.class);
    }

    @Override
    protected Predicate getSearchWherePredicate(String pattern, CriteriaBuilder builder, Root<Account> root) {
        return builder.or(
                builder.like(builder.lower(root.get("firstName")), pattern.toLowerCase() + "%"),
                builder.like(builder.lower(root.get("lastName")), pattern.toLowerCase() + "%"),
                builder.like(builder.lower(root.get("middleName")), pattern.toLowerCase() + "%"));
    }

    @Override
    public void delete(Account entity) {
        logger.info(START_METHOD + "delete");
        Account delAccount = entityManager.find(Account.class, entity.getId());
        entityManager.remove(delAccount);
        logger.debug(DELETE + entity);
    }

    public Account getByEmail(String email) {
        logger.info(START_METHOD + "getByEmail");
        if (email == null) {
            return null;
        }
        try {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Account> query = getEntityQuery(builder);
            Root<Account> from = getEntityRoot(query);
            query.select(from).where(builder.equal(from.get("email"), email));
            Account account = entityManager.createQuery(query).getSingleResult();
            logger.debug("Getting account: " + account);
            return account;
        } catch (NoResultException e) {
            logger.debug("Not account with email: " + email);
            return null;
        }
    }
}
