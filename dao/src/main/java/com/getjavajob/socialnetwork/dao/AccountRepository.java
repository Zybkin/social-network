package com.getjavajob.socialnetwork.dao;

import com.getjavajob.socialnetwork.common.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account getByEmail(String email);

    @Query("SELECT a FROM Account a WHERE a.firstName LIKE UPPER(CONCAT(:pattern, '%'))" +
            "OR a.lastName LIKE UPPER(CONCAT(:pattern, '%'))")
    Page<Account> getBySearchPattern(@Param("pattern") String pattern, Pageable pageable);

    @Query("SELECT a FROM Account a WHERE a.firstName LIKE UPPER(CONCAT(:pattern, '%'))" +
            "OR a.lastName LIKE UPPER(CONCAT(:pattern, '%'))")
    List<Account> getBySearchPatternAutocomplete(@Param("pattern") String pattern, Pageable pageable);
}
