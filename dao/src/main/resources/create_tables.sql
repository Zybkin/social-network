DROP TABLE IF EXISTS account_tbl;
DROP TABLE IF EXISTS phone_tbl;
DROP TABLE IF EXISTS friends_tbl;
DROP TABLE IF EXISTS group_tbl;

CREATE TABLE IF NOT EXISTS account_tbl (
  id                INT         NOT NULL AUTO_INCREMENT,
  last_name         VARCHAR(20) NOT NULL,
  first_name        VARCHAR(15) NOT NULL,
  middle_name       VARCHAR(20) NULL,
  birth_day         DATE        NULL,
  home_address      VARCHAR(40) NULL,
  work_address      VARCHAR(40) NULL,
  email             VARCHAR(30) NOT NULL UNIQUE,
  password          CHAR(65)    NOT NULL,
  skype             VARCHAR(30) NULL,
  telegram          VARCHAR(30) NULL,
  add_info          TEXT        NULL,
  registration_date TIMESTAMP            DEFAULT CURRENT_TIMESTAMP,
  avatar            LONGBLOB    NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS phone_tbl (
  id         INT         NOT NULL AUTO_INCREMENT,
  account_id INT         NOT NULL,
  number     VARCHAR(30) NOT NULL UNIQUE,
  FOREIGN KEY (account_id) REFERENCES account_tbl (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS friends_tbl (
  account_id INT NOT NULL,
  friend_id  INT NOT NULL,
  response BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (account_id) REFERENCES account_tbl (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (friend_id) REFERENCES account_tbl (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  PRIMARY KEY (account_id, friend_id)
);

--  need to create
-- names for constraints
--  registration_date
--  primary keys
--  foreign keys
--  registration_date	TIMESTAMP		NULL DEFAULT CURRENT_TIMESTAMP,
--  'active', 'picture'

-- CREATE TABLE IF NOT EXISTS 	group_tbl (
--   id				INT 			NOT NULL AUTO_INCREMENT,
--   name				VARCHAR(255)	NOT NULL UNIQUE,
--   owner				INT				NOT NULL,
--   add_info			TEXT			NULL,
--   FOREIGN KEY (owner) REFERENCES account_tbl (id),
--   PRIMARY KEY (id)
-- );

-- CREATE TABLE IF NOT EXISTS group_member_tbl (
--   group_id
--   member_id
--   admin
-- );


