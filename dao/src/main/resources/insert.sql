
-- ACCOUNTS
INSERT INTO account_tbl (last_name, first_name, birth_day, email, password)
VALUES ('Salvador', 'Dali', '1904-05-11', '123', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3');

INSERT INTO account_tbl (last_name, first_name, birth_day, email, password)
VALUES ('Mercury', 'Freddie', '1946-09-05', '124', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3');

INSERT INTO account_tbl (last_name, first_name, birth_day, email, password)
VALUES ('Guevara', 'Che', '1928-06-14', '125', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3');

INSERT INTO account_tbl (last_name, first_name, birth_day, email, password)
VALUES ('Monroe', 'Marilyn', '1926-06-1', '126', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3');


-- FRIENDS
INSERT INTO friends_tbl VALUES(1,2);
INSERT INTO friends_tbl VALUES(2,4);

-- PHONE_TBL
INSERT INTO phone_tbl (account_id, number) VALUES (1, '123');
INSERT INTO phone_tbl (account_id, number) VALUES (2, '124');
INSERT INTO phone_tbl (account_id, number) VALUES (3, '125');
INSERT INTO phone_tbl (account_id, number) VALUES (4, '126');
